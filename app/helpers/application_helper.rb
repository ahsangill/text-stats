module ApplicationHelper
  def font_sizes
    (1..30).to_a
  end

  def font_faces
    %w(Tizen\ Sans\ Regular)
  end
end
