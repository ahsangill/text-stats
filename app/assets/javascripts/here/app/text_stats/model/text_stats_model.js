(function($) {

  if (!Here.Apps.TextStats) { Here.Apps.TextStats = {} }

  Here.Apps.TextStats.Collection = Backbone.Collection.extend({

    rootUrl: '/stats/calculate',

    url: function() {
      var requestUrl = this.rootUrl;
      if (this.data) {
        requestUrl += this.toQueryString(this.data);
      }
      return requestUrl;
    },

    /*
     This takes a hash as input and maps it to query string format

     Example:   {name: 'ABC', address: 'XYZ'} =>  "?name='ABC'&address='XYZ'"

     */
    toQueryString: function(data){
      var query_string = "?"

      for (var key in data) {
        if(query_string != "?"){
          query_string += "&"
        }
        query_string +=  (key + '=' + data[key]);
      }

      return query_string;
    },

    parse: function(response) {
      return response;
    },

    calculate: function(data, callbacks) {
      this.data = data;
      return this.fetch(callbacks);
    },

  });

}(jQuery))