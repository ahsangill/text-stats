(function($) {

  if (!Here.Apps.TextStats) { Here.Apps.TextStats = {} }

  Here.Apps.TextStats.App = Here.Libs.App.extend({

    autoPositioning: false,

    init: function(options) {
      this.name = 'TextStatsApp';
      this.form_view = new Here.Apps.TextStats.Views.FormView({ hereRef: options.hereRef });
    },

    activate: function() {
      this.form_view.render();
    }

  });

}(jQuery))