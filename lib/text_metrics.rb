class TextMetrics

  attr :text, :font_face, :font_size, :max_width

  def initialize(text, font_face, font_size, max_width)
    @text = text
    @font_face = font_face
    @font_size = font_size.to_i
    @max_width = max_width.to_i
  end


  # Public: Calculates the number of lines against a given +max_width+ if the text is wrapped
  #
  # Returns an Integer equal to the number of lines
  def no_of_lines
    if width_of(text) > max_width
      lines_count = 1
      line_in_text = ''

      words_in_text.each_with_index do |word, index|
        if width_of(word) > max_width
          lines_count += 1 if index != 0
          line_in_text = ''

          word.chars.each_with_index do |ch, ch_index|
            line_in_text += ch

            if width_of(line_in_text) > max_width
              line_in_text = if word.length != ch_index + 1 && line_in_text.split.size > 1
                               line_in_text.split[-1]
                             else
                               lines_count += 1
                               ch
                             end
            end
          end
        else
          line_in_text = join_strings(line_in_text, word)

          if width_of(line_in_text) > max_width
            lines_count += 1

            line_in_text = if word.blank? && line_in_text.split.size > 1
                             get_last_word(line_in_text)
                           else
                             word
                           end
          end
        end
      end

      lines_count
    else

      text.empty? ? 0 : 1
    end
  end


  # Public: Calculates the text width of a given +text+
  #
  # Returns an Integer equal to the width of the text
  def text_width
    width_of(text)
  end


  protected

  def get_last_word(str)
    no_more_whitespace = false
    return_str = ""
    str.chars.reverse.each do |ch|
      if ch != ' '
        no_more_whitespace = true
      end
      return return_str.reverse if no_more_whitespace && ch == ' '
      return_str << ch
    end
  end

  # Protected: Measures the width of the given text in points
  #
  # Returns an Integer containing the width of the passed string
  def width_of(str)
    document.width_of(str).ceil
  end


  # Protected: Splits the text into words
  #
  # Returns an Array of strings
  def join_strings(line_in_text, word)
    [line_in_text, word].join(' ')
  end


  # Protected: Splits the text into words
  #
  # Returns an Array of strings
  def words_in_text
    text.split(/\ /, -1)
  end


  # Protected: Instantiates a singleton object of <tt>Prawn::Document</tt> and sets its font face and font size.
  #
  # Returns an object of <tt>Prawn::Document.new</tt>
  def document
    @document ||= Prawn::Document.new
    @document.font(get_font_path(font_face))
    @document.font_size(font_size)
    @document
  end


  # Protected:
  #
  # Returns a string containing the source location of a given font-family
  def get_font_path(font_face)
    if font_face == 'Tizen Sans Regular'
      # TODO: Move this to a YAML file and read it from there.
      "#{Rails.root}/app/assets/fonts/TizenSansRegular.ttf"
    else
      raise 'Font not found'
    end
  end
end