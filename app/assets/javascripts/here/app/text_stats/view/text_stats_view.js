(function($) {

  if (!Here.Apps.TextStats.Views) {Here.Apps.TextStats.Views = {}}

  Here.Apps.TextStats.Views.FormView = Backbone.View.extend({

    el: '#text_stats_app',

    events: {
      'click #get_stats':        'actionPerformCalculations'
    },

    initialize: function(options) {
      this.hereRef = options.hereRef;
      this.collection = new Here.Apps.TextStats.Collection();
    },

    render: function() {
      var template = _.template($('#text_stats_main_template').html(), this.collection.attributes);
      this.$el.html(template);
      return this;
    },

    actionPerformCalculations: function(event) {

      var self = this;
      event.preventDefault();

      /*
        Read values from UI input fields and store them in a Hash format.
       */
      var data = {  "text"      :  $('#text').val(),
                    "font_face" :  $('#font_face').val(),
                    "font_size" :  $('#font_size').val(),
                    "max_width" :  $('#max_width').val()
                  }

      /*
         Perform calculations on the selected and provided input data
       */
      this.collection.calculate(data, {
        beforeSend: function() {
          //return self.validate();
        },
        success: function(model, response) {
          self.showResults(response);
        },
        error: function(model, response) {
        }
      });
    },

    // TODO: Implemented front-end input validations
    validate: function(){
      return true;
    },

    /*
       Display results to the user
     */
    showResults: function(response) {
      var self = this;
      $('.width').html("Width: " + response['text_width'])
      $('.lines').html("Number of lines: " + response['no_of_lines'])
    }

  });

}(jQuery))