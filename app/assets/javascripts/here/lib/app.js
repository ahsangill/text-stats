(function($) {
  if (!window.Here) {
    Here = {
      Apps: {},
      Libs: {}
    }
  }

  Here.Libs.App = Class.extend({
    init: function(options) {
      this.name = 'app';
      this.hereRef = options.hereRef;
    },

    activate: function() {
    },

    deactivate: function() {
    }
  });

}(jQuery))