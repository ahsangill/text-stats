require "rails_helper"

describe StatsController do

  describe 'GET index' do
    it 'has a success status code' do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'Get text stats' do
    before :each do
      @params = {
        format: 'json',
        font_face: 'Tizen Sans Regular',
        font_size: '10',
        max_width: '150',
        text: 'At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.'
      }
    end

    context 'with a regular string' do
      it 'should show calculated values' do
        get :calculate, @params

        json_response = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(json_response['text_width']).to eq(515)
        expect(json_response['no_of_lines']).to eq(4)
      end
    end

    context 'with completely blank text' do
      it 'should show calculated values' do
        @params[:text] = "                                                            "

        get :calculate, @params

        json_response = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(json_response['text_width']).to eq(162)
        expect(json_response['no_of_lines']).to eq(2)
      end
    end

    context 'with multiple spaces in text' do
      it 'should show calculated values' do
        @params[:text] = 'This         is not a regular                                   string.'

        get :calculate, @params

        json_response = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(json_response['text_width']).to eq(231)
        expect(json_response['no_of_lines']).to eq(3)
      end
    end

    context 'with very long words in text' do
      it 'should show calculated values' do
        @params[:text] = 'Atw3schools.comyouwilllearnhowtomake a website. Weofferfreetutorialsinallwebdevelopment technologies.'

        get :calculate, @params

        json_response = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(json_response['text_width']).to eq(477)
        expect(json_response['no_of_lines']).to eq(4)
      end
    end

    context 'with whitespace at the end of the text' do
      it 'should show calculated values' do
        @params[:text] = 'Atw3schools.comyouwilllearnhowtomake a website. Weofferfreetutorialsinallwebdevelopment technologies.                                                '

        get :calculate, @params

        json_response = JSON.parse(response.body)

        expect(response.status).to eq(200)
        expect(json_response['text_width']).to eq(607)
        expect(json_response['no_of_lines']).to eq(6)
      end
    end

  end
end
