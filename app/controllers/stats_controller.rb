class StatsController < ApplicationController

  def index

  end

  def calculate

    text_metrics = TextMetrics.new(params[:text], params[:font_face], params[:font_size], params[:max_width])
    response = {
      no_of_lines: text_metrics.no_of_lines,
      text_width: text_metrics.text_width
    }

    respond_to do |format|
      format.json { render :json => response, :status => '200' }
    end

  end
end
